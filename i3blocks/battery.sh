#!/bin/bash

BAT=$(acpi -b | grep -E -o '[0-9]*[0-9][0-9]?%')

# Full and short texts
if [ ${BAT%?} -le 5 ]
then
	echo ": $BAT" # Empty
elif [ ${BAT%?} -le 25 ]
then
	echo ": $BAT" # One quarter
elif [ ${BAT%?} -le 50 ]
then
	echo ": $BAT" # Half
elif [ ${BAT%?} -le 75 ]
then
	echo ": $BAT" # Three quarters
else
	echo ": $BAT" # Full
fi

# Set urgent flag below 5% or use orange below 20%
[ ${BAT%?} -le 5 ] && exit 33
[ ${BAT%?} -le 20 ] && echo "#FF8000"

exit 0
