#!/bin/bash

VOL=$(pamixer --get-volume)

MUT=$(pamixer --get-mute)

if [ "$MUT" = true ]; then
	echo ": $VOL"
else
	echo ": $VOL"
fi

exit 0
