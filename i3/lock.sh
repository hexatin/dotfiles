#!/bin/bash

#icon="$HOME/.config/i3/lock.png"
img="$HOME/.cache/i3lock.png"

scrot $img
# Pixelate image
#convert $img -scale 10% -scale 1000% $img
# Blur image
convert $img -blur 0x10 500% -modulate 100,150 $img
#convert $img $icon -gravity center -composite $img
i3lock -ef -i $img

rm $img
