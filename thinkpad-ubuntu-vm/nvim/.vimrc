" basic
set encoding=utf-8
cd ~/Documents/
set backspace=indent,eol,start

" remapping
nmap <Leader>s :source $MYVIMRC
nmap <Leader>v :tabe $MYVIMRC

set cc=80,100,120
set mouse=a " allows mouse scrolling
set number
set undofile
set undodir=~/tmp/vim/undo//
" set directory=~/tmp/vim/swap//
set nowrap
set shiftwidth=4
set tabstop=4
set nohlsearch

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" vim-plug
call plug#begin()
Plug 'vim-airline/vim-airline'
Plug 'crusoexia/vim-monokai'
Plug 'dracula/vim',{'as':'dracula'}
Plug 'kamwitsta/flatwhite-vim'
Plug 'drewtempelmeyer/palenight.vim'
Plug 'trusktr/seti.vim'
Plug 'Yavor-Ivanov/airline-monokai-subtle.vim'
Plug 'godlygeek/csapprox'
" Plug 'vim-syntastic/syntastic'
Plug 'tpope/vim-surround'
"Plug 'ervandew/supertab'
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }
" (Optional) Multi-entry selection UI.
Plug 'junegunn/fzf'
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'ncm2/ncm2'
Plug 'roxma/nvim-yarp'
Plug 'jiangmiao/auto-pairs'

" More ncm2 completion sources
Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-path'
Plug 'ncm2/ncm2-vim'
Plug 'ncm2/ncm2-jedi'
Plug 'Shougo/neco-vim'
call plug#end()

" airline config
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#syntastic#enabled = 1




" aesthetics
set t_Co=256
set background=dark
colorscheme dracula
let g:airline_theme = 'monokai_subtle'
let g:monokai_term_italic = 1
let g:monokai_gui_italic = 1
let g:palenight_terminal_italics=1
set termguicolors
autocmd VimEnter * RainbowParentheses .

" Enable True Colors
if (has("nvim"))
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif




" NCM2

" enable ncm2 for all buffers
autocmd BufEnter * call ncm2#enable_for_buffer()

" IMPORTANT: :help Ncm2PopupOpen for more information
set completeopt=noinsert,menuone,noselect

inoremap <expr><Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr><S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

let g:ncm2#popup_delay=20

" RemoveLC python from the ncm2 sources to use jedi instead
call ncm2#override_source('LanguageClient_python', {'enable': 0})



" LANGUAGE CLIENT

" Required for operations modifying multiple buffers like rename.
set hidden


let g:LanguageClient_serverCommands = {
    \ 'python': ['pyls', '-v'],
    \ 'c': ['ccls', '--log-file=/tmp/cc.log'],
    \ 'cpp': ['ccls', '--log-file=/tmp/cc.log'],
    \ 'cuda': ['ccls', '--log-file=/tmp/cc.log'],
    \ 'objc': ['ccls', '--log-file=/tmp/cc.log'],
    \ }

" Automatically start language servers.
let g:LanguageClient_autoStart = 1

nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

setl formatexpr=LanguageClient#textDocument_rangeFormatting()


" CCLS Settings
let g:LanguageClient_loadSettings = 1 " Use an absolute configuration path if you want system-wide settings
let g:LanguageClient_settingsPath = '/home/hexatin/.config/nvim/settings.json'
" https://github.com/autozimu/LanguageClient-neovim/issues/379 LSP snippet is not supported
let g:LanguageClient_hasSnippetSupport = 0


" CCLS formatting
fu! C_init()
	setl formatexpr=LanguageClient#textDocument_rangeFormatting()
	" Semantic navigation
	nn <silent> xh :call LanguageClient#findLocations({'method':'$ccls/navigate','direction':'L'})<cr>
	nn <silent> xj :call LanguageClient#findLocations({'method':'$ccls/navigate','direction':'D'})<cr>
	nn <silent> xk :call LanguageClient#findLocations({'method':'$ccls/navigate','direction':'U'})<cr>
	nn <silent> xl :call LanguageClient#findLocations({'method':'$ccls/navigate','direction':'R'})<cr>
endf
au FileType c,cpp,cuda,objc :call C_init()

" TODO get zsh

"colorscheme dracula
"#highlight Normal ctermbg=None
"if (has("termguicolors"))
  "set termguicolors
"endif
" set Vim-specific sequences for RGB colors
"let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
"let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
